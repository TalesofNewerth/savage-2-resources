// (C)2008 S2 Games
// simple.vsh
// 
// Default null vertex shader
//=============================================================================

//=============================================================================
// Uniform variables
//=============================================================================

//=============================================================================
// Vertex shader
//=============================================================================
void main()
{
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
}
